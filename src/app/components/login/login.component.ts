import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../../services/authentication/authentication.service';
import {Router} from '@angular/router';
import {User} from '../../models/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  model: User = new User();
  loading = false;
  error = '';

  constructor(private router: Router, private authService: AuthenticationService) {  }

  ngOnInit() {
    this.authService.logout();
  }

  login() {
    this.loading = true;
    console.log(this.model.email, this.model.password);

    this.authService.login(this.model.email, this.model.password).subscribe((result) => {
      if (result === true) {
        this.router.navigate(['/']);
      } else {
        this.error = 'Username or password is incorrect';
        this.loading = false;
      }
    });
  }

}
