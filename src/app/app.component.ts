import {Component, OnInit} from '@angular/core';
import {GroupService} from './services/group/group.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app works!';
  private svc: GroupService;
  private groups: any[];

  constructor(groupService: GroupService) {
    this.svc = groupService;
  }

  ngOnInit() {
    console.log('Init');
    // this.svc.getAll().then((data) => {
    //   this.groups = data;
    //   console.log(data);
    // });
  }
}
