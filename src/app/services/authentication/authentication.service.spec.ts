/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { AuthenticationService } from './authentication.service';
import {HttpModule} from '@angular/http';

describe('AuthenticationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpModule
      ],
      providers: [AuthenticationService]
    });
  });

  it('should construct, login and logout', inject([AuthenticationService], (service: AuthenticationService) => {
    expect(service).toBeTruthy();
    const value = service.login('example@mail.com', '123123123');
    value.subscribe((result: Boolean) => {
      expect(result).toBe(true);
      expect(service.token).toBeDefined();
      service.logout();
      expect(service.token).toBeNull();
    });
  }));
});
