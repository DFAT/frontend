import { Injectable } from '@angular/core';
import {Headers, Http, Response} from '@angular/http';
import {Observable} from 'rxjs/observable';
import {Config} from '../../config';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthenticationService {
  public token: string;
  private authUrl: string;

  constructor(private http: Http) {
    this.authUrl = Config.apiUrl().concat('/api/authenticate');
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.token = currentUser && currentUser.token;
  }

  login(email: string, password: string): Observable<boolean> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.authUrl, JSON.stringify({
      email: email,
      password: password
    }), {headers: headers}).map((response: Response) => {
      console.log(response);
      const token = response.json() && response.json().auth_token;
      console.log(token);
      if (token) {
        this.token = token;

        localStorage.setItem('currentUser', JSON.stringify({
          email: email,
          token: token
        }));
        return true;
      } else {
        return false;
      }
    });
  }

  logout(): void {
    this.token = null;
    localStorage.removeItem('currentUser');
  }
}
