import { Injectable } from '@angular/core';
import {Headers, Http, Response} from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class GroupService {
  private apiUrl: string;
  private headers: Headers;

  constructor(private http: Http) {
    this.apiUrl = 'http://localhost:3000/api/groups';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
  }

  public getAll(): Promise<any[]> {
    return this.http.get(this.apiUrl)
      .toPromise()
      .then((response: Response) => <any[]>response.json())
      .catch(this.handleError);
  }

  private handleError(error: Response): Promise<any> {
    console.error(error);
    return Promise.reject(error.json().error || 'Server error');
  }

}
