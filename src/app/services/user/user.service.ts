import { Injectable } from '@angular/core';
import {AuthenticationService} from '../authentication/authentication.service';
import {Headers, Http, RequestOptions, Response} from '@angular/http';
import {Observable} from 'rxjs/observable';
import {User} from '../../models/user';
import {Config} from '../../config';

@Injectable()
export class UserService {
  private userUrl: string;

  constructor(private http: Http, private authService: AuthenticationService) {
    this.userUrl = Config.apiUrl().concat('/api/users');
  }

  getUsers(): Observable<User[]> {
    const headers = new Headers({ 'Authorization': `Bearer ${this.authService.token}`});
    const options = new RequestOptions({headers: headers});

    return this.http.get(this.userUrl, options).map((response: Response) => response.json());
  }
}
