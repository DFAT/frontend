/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { UserService } from './user.service';
import {HttpModule} from '@angular/http';
import {AuthenticationService} from '../authentication/authentication.service';
import { User } from '../../models/user';

describe('UserService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpModule
      ],
      providers: [
        UserService,
        AuthenticationService
      ]
    });
  });

  it('should construct, authenticate and get a list of users', inject([UserService, AuthenticationService],
    (userService: UserService, authService: AuthenticationService) => {
      expect(userService).toBeTruthy();
      expect(authService).toBeTruthy();
      const value = authService.login('example@mail.com', '123123123');
      value.subscribe((result: Boolean) => {
        expect(result).toBeTruthy();
        userService.getUsers().subscribe((res: User[]) => {
          expect(res.length).toBe(1);
        });
      });
  }));
});
